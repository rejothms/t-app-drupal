<?php

/**
 * @file
 * Common Module.
 */
function _authentication_signup($args)
{
  $account = array();
  $count = 0;
  $data_type = 'user';
  //Initialise result array
  $result = array();
  $username = common_get_array_data($args, 'username');
  $password = common_get_array_data($args, 'password');
  $mail = common_get_array_data($args, 'mail');
  $role = common_get_array_data($args, 'role');
  if ($role != "") {
    $rid = array_search($role, user_roles());
    $roles = array($rid => $role);
  }
  $role_arr = common_get_array_data($args, 'roles');
  if (is_array($role_arr)) {
    foreach ($role_arr as $key => $role_name) {
      $rid = array_search($role_name, user_roles());
      if ($rid != "") {
        $roles[$rid] = $role_name;
      }
    }
  }
  $result['data'][$data_type][$count] = array();
  if (($username == "") || ($password == "")) {
    common_set_service_footer($result, $data_type, null, $message = "Please provide mandatory feilds ('password','username')", 400);
  } else if (user_load_by_name($username)) {
    common_set_service_footer($result, $data_type, null, $message = "Username already exist. Please try different username.", 409);
  } else {

    if (($username == "") && ($mail != "")) {
      $username = $mail;
    }

    $new_user = array(
      'name' => $username,
      'pass' => $password,
      'mail' => $mail,
      'status' => 1,
      'created' => REQUEST_TIME,
      'access' => REQUEST_TIME,
      'roles' => $roles,
    );
    if (common_get_array_data($args, 'first_name') != "") {
      $new_user['field_first_name'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'first_name');
    }
    if (common_get_array_data($args, 'last_name') != "") {
      $new_user['field_last_name'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'last_name');
    }
    if (common_get_array_data($args, 'flat_number') != "") {
      $new_user['field_flat_num'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'flat_number');
    }
    if (common_get_array_data($args, 'room_number') != "") {
      $new_user['field_room_num'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'room_number');
    }
    if (common_get_array_data($args, 'customer_name') != "") {
      $new_user['field_customer_name'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_name');
    }
    if (common_get_array_data($args, 'customer_name_ar') != "") {
      $new_user['field_customer_name_ar'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_name_ar');
    }
    if (common_get_array_data($args, 'customer_id') != "") {
      $new_user['field_customer_id'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_id');
    }
    if (common_get_array_data($args, 'occupant_name') != "") {
      $new_user['field_occupant_name'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'occupant_name');
    }
    if (common_get_array_data($args, 'occupant_name_ar') != "") {
      $new_user['field_occupant_name_ar'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'occupant_name_ar');
    }
    if (common_get_array_data($args, 'employer_name') != "") {
      $new_user['field_employer_name'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'employer_name');
    }
    if (common_get_array_data($args, 'employer_name_ar') != "") {
      $new_user['field_employer_name_ar'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'employer_name_ar');
    }
    if (common_get_array_data($args, 'postbox') != "") {
      $new_user['field_postbox'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'postbox');
    }
    if (common_get_array_data($args, 'phone_res') != "") {
      $new_user['field_phone_res'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'phone_res');
    }
    if (common_get_array_data($args, 'phone_wrk') != "") {
      $new_user['field_phone_wrk'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'phone_wrk');
    }
    if (common_get_array_data($args, 'mobile') != "") {
      $new_user['field_mobile'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'mobile');
    }
    if (common_get_array_data($args, 'fax') != "") {
      $new_user['field_fax'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'fax');
    }
    if (common_get_array_data($args, 'email') != "") {
      $new_user['field_email'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'email');
    }
    if (common_get_array_data($args, 'country') != "") {
      $new_user['field_country'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'country');
    }
    if (common_get_array_data($args, 'device_api‎') != "") {
      $new_user['field_device_api‎'][LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'device_api‎');
    }
    $field_collection_values = array();
    $cnt = 0;
    if (array_key_exists('attachments', $args)) {
      foreach ($args['attachments'] as $key => $attachments) {
        $key = str_replace(array("'"), '', $key);
        $field_collection_values[$cnt]['field_name'] = "field_" . $key;
        foreach ($attachments as $akey => $avalue) {
          $akey = str_replace(array("'"), '', $akey);
          $field_collection_values[$cnt]["field_" . $akey][LANGUAGE_NONE][0]['value'] = trim($avalue);
        }
        if (array_key_exists($key, $_FILES)) {
          $entity_type = 'field_collection_item';
          $field_name = 'field_attachment';
          $entity_name = "field_" . $key;
          $file_temp = common_file_save_upload($field_name, $entity_type, $entity_name, $key, $key);
          $field_collection_values[$cnt]['field_attachment'][LANGUAGE_NONE] = $file_temp;
        }
        $cnt++;
      }
    }
    $account->is_new = true;
    $user_data = user_save($account, $new_user);
    if (count($field_collection_values) > 0) {
      foreach ($field_collection_values as $field_collection_value) {
        $field_entity = entity_create('field_collection_item', $field_collection_value);
        $field_entity->setHostEntity('user', $user_data);
        $field_entity->save();
      }
    }
    $result['data'][$data_type][$count] = get_site_user($user_data);
    common_set_service_footer($result, $data_type, null, $message = "Successfully created user.", 200);
  }
  return $result;
}

function _authentication_signin($args)
{
  /*
    {
    "username" : "santhosh",
    "password":"santhosh",
    }
   */
  $account = array();
  $count = 0;
  $data_type = 'user';
  //Initialise result array
  $result = array();
  $username = common_get_array_data($args, 'username');
  $password = common_get_array_data($args, 'password');
  $mail = common_get_array_data($args, 'mail');
  if (($username == "") || ($password == "")) {
    common_set_service_footer($result, $data_type, null, $message = "Please provide mandatory feilds ('password','username')", 400);
  }
  if (($username == "") && ($mail != "")) {
    $username = $mail;
  }
  if (user_authenticate($username, $password)) {
    $account = user_load_by_name($username);
    $result['data'][$data_type][$count] = get_site_user($account);
    common_set_service_footer($result, $data_type, null, $message = "Successfully retrieved user data.", 200);
  }
  if (empty($account)) {
    common_set_service_footer($result, $data_type, null, $message = "Invalid credentials", 400);
  }
  return $result;
}

function _authentication_update_user($args)
{
  $account = array();
  $user_data = common_get_user($args);
  $password = common_get_array_data($args, 'password');
  $username = common_get_array_data($args, 'username');
  if ($username != "") {
    $check_user_data = user_load_by_name($username);
    if (($check_user_data != false) && ($check_user_data->uid != $user_data->uid)) {
      header('HTTP/1.1 409 Conflict', true, 409);
      $result['error'] = "Username already exist for another user. Please try different username.";
      return $result;
    } else {
      $user_data->username = $username;
    }
  }

  $mail = common_get_array_data($args, 'mail');
  if ($mail != "") {
    $check_user_data = user_load_by_mail($mail);
    if (($check_user_data != false) && ($check_user_data->uid != $user_data->uid)) {
      header('HTTP/1.1 409 Conflict', true, 409);
      $result['error'] = "Email already exist for another user. Please try different username.";
      return $result;
    } else {
      $user_data->mail = $mail;
    }
  }
  if ($password != "") {
    $user_data->pass = $password;
  }
  if (common_get_array_data($args, 'role') != "") {
    $role = common_get_array_data($args, 'role');
    $rid = array_search($role, user_roles());
    $roles = array($rid => $role);
    $user_data->roles = $roles;
  }
  if (common_get_array_data($args, 'first_name') != "") {
    $user_data->field_first_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'first_name');
  }
  if (common_get_array_data($args, 'last_name') != "") {
    $user_data->field_last_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'last_name');
  }
  if (common_get_array_data($args, 'flat_number') != "") {
    $user_data->field_flat_num[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'flat_number');
  }
  if (common_get_array_data($args, 'room_number') != "") {
    $user_data->field_room_num[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'room_number');
  }
  if (common_get_array_data($args, 'customer_name') != "") {
    $user_data->field_customer_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_name');
  }
  if (common_get_array_data($args, 'customer_name_ar') != "") {
    $user_data->field_customer_name_ar[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_name_ar');
  }
  if (common_get_array_data($args, 'customer_id') != "") {
    $user_data->field_customer_id[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_id');
  }
  if (common_get_array_data($args, 'occupant_name') != "") {
    $user_data->field_occupant_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'occupant_name');
  }
  if (common_get_array_data($args, 'occupant_name_ar') != "") {
    $user_data->field_occupant_name_ar[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'occupant_name_ar');
  }
  if (common_get_array_data($args, 'employer_name') != "") {
    $user_data->field_employer_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'employer_name');
  }
  if (common_get_array_data($args, 'employer_name_ar') != "") {
    $user_data->field_employer_name_ar[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'employer_name_ar');
  }
  if (common_get_array_data($args, 'postbox') != "") {
    $user_data->field_postbox[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'postbox');
  }
  if (common_get_array_data($args, 'phone_res') != "") {
    $user_data->field_phone_res[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'phone_res');
  }
  if (common_get_array_data($args, 'phone_wrk') != "") {
    $user_data->field_phone_wrk[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'phone_wrk');
  }
  if (common_get_array_data($args, 'mobile') != "") {
    $user_data->field_mobile[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'mobile');
  }
  if (common_get_array_data($args, 'fax') != "") {
    $user_data->field_fax[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'fax');
  }
  if (common_get_array_data($args, 'email') != "") {
    $user_data->field_email[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'email');
  }
  if (common_get_array_data($args, 'country') != "") {
    $user_data->field_country[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'country');
  }
  if (common_get_array_data($args, 'device_api') != "") {
    $user_data->field_device_api[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'device_api');
  }
  if (array_key_exists('attachments', $args)) {
    foreach ($args['attachments'] as $key => $attachments) {
      $key = str_replace(array("'"), '', $key);
      $field_collection_item_id = common_get_field_data($user_data, "field_" . $key, $lan_code);
      $field_collection_value = field_collection_item_load($field_collection_item_id);
      foreach ($attachments as $akey => $avalue) {
        $akey = str_replace(array("'"), '', $akey);
        if ($avalue != "") {
          $field_collection_value->{'field_' . $akey}[LANGUAGE_NONE][0]['value'] = trim($avalue);
          $field_collection_value->save();
        }
      }
    }
  }

  $field_collection_items = array('passport', 'visa', 'license', 'power_attorney');
  foreach ($field_collection_items as $field_collection_item) {
    if (array_key_exists($field_collection_item, $_FILES)) {
      $entity_type = 'field_collection_item';
      $field_name = 'field_attachment';
      $entity_name = "field_" . $field_collection_item;
      $file_temp = common_file_save_upload($field_name, $entity_type, $entity_name, $field_collection_item, $field_collection_item);
      $field_collection_item_id = common_get_field_data($user_data, "field_" . $field_collection_item, $lan_code);
      $field_collection_value = field_collection_item_load($field_collection_item_id);
      $field_collection_value->field_attachment[LANGUAGE_NONE] = $file_temp;
      $field_collection_value->save();
    }
  }
  $account = user_save($user_data);
  $result = get_site_user($account);
  return $result;
}

function _authentication_get_user($args)
{
  $user_data = common_get_user($args);
  $result = get_site_user($user_data);
  return $result;
}

function get_site_user($account)
{
  if (isset($account->uid)) {
    $form_state = array();
    $form_state['uid'] = $account->uid;
    user_login_submit(array(), $form_state);
  }
  global $user;
  $lan_code = common_get_language();
  $site_user = array();
  $site_user['first_name'] = common_get_field_data($account, 'field_first_name', $lan_code);
  $site_user['last_name'] = common_get_field_data($account, 'field_last_name', $lan_code);
  $site_user['mail'] = common_get_property_data($account, 'mail');
  $site_user['phone_number'] = common_get_field_data($account, 'field_phone_number', $lan_code);
  $site_user['image'] = (isset($account->picture->uri)) ? $account->picture->uri : "";
  $site_user['roles'] = (isset($account->roles)) ? $account->roles : "";
  $site_user['flat_number'] = common_get_field_data($account, 'field_flat_num', $lan_code);
  $site_user['room_number'] = common_get_field_data($account, 'field_room_num', $lan_code);
  $site_user['customer_name'] = common_get_field_data($account, 'field_customer_name', $lan_code);
  $site_user['customer_name_ar'] = common_get_field_data($account, 'field_customer_name_ar', $lan_code);
  $site_user['customer_id'] = common_get_field_data($account, 'field_customer_id', $lan_code);
  $site_user['occupant_name'] = common_get_field_data($account, 'field_occupant_name', $lan_code);
  $site_user['occupant_name_ar'] = common_get_field_data($account, 'field_occupant_name_ar', $lan_code);
  $site_user['employer_name'] = common_get_field_data($account, 'field_employer_name', $lan_code);
  $site_user['employer_name_ar'] = common_get_field_data($account, 'field_employer_name_ar', $lan_code);
  $site_user['postbox'] = common_get_field_data($account, 'field_postbox', $lan_code);
  $site_user['phone_res'] = common_get_field_data($account, 'field_phone_res', $lan_code);
  $site_user['phone_wrk'] = common_get_field_data($account, 'field_phone_wrk', $lan_code);
  $site_user['mobile'] = common_get_field_data($account, 'field_mobile', $lan_code);
  $site_user['fax'] = common_get_field_data($account, 'field_fax', $lan_code);
  $site_user['email'] = common_get_field_data($account, 'field_email', $lan_code);
  $site_user['device_api'] = common_get_field_data($account, 'field_device_api', $lan_code);
  $site_user['country'] = common_get_field_data($account, 'field_country', $lan_code);
  $site_user['session_token'] = drupal_get_token('services');
  $encrypted_user_id = base64_encode($user->uid);
  $site_user['user_id'] = $encrypted_user_id;
  $owned_customer_rid = array_search('owned_customer', $account->roles);
  if($owned_customer_rid != "") {
    $site_user['tenancy'] = get_tenancy_by_user($user->uid);
  }
  $field_collection_items = array('passport', 'visa', 'license', 'power_attorney');
  foreach ($field_collection_items as $field_collection_item) {
    $item_id = common_get_field_data($account, 'field_' . $field_collection_item, $lan_code);
    $collection = entity_load('field_collection_item', array($item_id));
    $site_user['attachments'][$field_collection_item] = array();
    $site_user['attachments'][$field_collection_item]['number'] = common_get_field_data($collection[$item_id], 'number', $lan_code);
    $photos = common_get_field_data($collection[$item_id], 'attachment', $lan_code, 'filename', 'all');
    foreach ($photos as $index => $photo) {
      $site_user['attachments'][$field_collection_item]['files'][$index]['original'] = file_create_url($photo['uri']);
    }
    $site_user['attachments'][$field_collection_item]['issue_date'] = common_get_field_data($collection[$item_id], 'issue_date', $lan_code);
    $site_user['attachments'][$field_collection_item]['expiry_date'] = common_get_field_data($collection[$item_id], 'expiry_date', $lan_code);
    $site_user['attachments'][$field_collection_item]['remind_before'] = common_get_field_data($collection[$item_id], 'remind_before', $lan_code);
    if ($field_collection_item == 'passport') {
      $site_user['attachments'][$field_collection_item]['issue_place'] = common_get_field_data($collection[$item_id], 'issue_place', $lan_code);
    }
  }
  return $site_user;
}

function get_site_user_list_by_role($role_name, $list_type = 'all', $count_flag = 0)
{
  global $language;
  $lan_code = $language->language;
  $data_type = 'users';
  //Initialise result array
  $result = array();
  $rid = array_search($role_name, user_roles());
  $query = db_select('users_roles', 'users_roles');
  $query->distinct();
  $query->fields('users_roles', array('uid'));
  $query->condition('rid', $rid, '=');
  $uids = $query->execute()->fetchCol();
  if ($count_flag == 1) {
    return count($uids);
  }
  if (!empty($uids)) {
    $users_list = user_load_multiple($uids);
    $count = 0;
    $result['data'][$data_type][$count] = array();
    foreach ($users_list as $account) {
      $encrypted_user_id = base64_encode($account->uid);
      if ($list_type == 'minimal') {
        $result['data'][$data_type][$count]['uid'] = base64_encode($account->uid);
        $result['data'][$data_type][$count]['username'] = $account->name;
      } else {
        $result['data'][$data_type][$count]['user_id'] = $encrypted_user_id;
        $result['data'][$data_type][$count]['username'] = $account->name;
        $result['data'][$data_type][$count]['first_name'] = common_get_field_data($account, 'field_first_name', $lan_code);
        $result['data'][$data_type][$count]['last_name'] = common_get_field_data($account, 'field_last_name', $lan_code);
        $result['data'][$data_type][$count]['mail'] = common_get_property_data($account, 'mail');
        $result['data'][$data_type][$count]['phone_number'] = common_get_field_data($account, 'field_phone_number', $lan_code);
        $result['data'][$data_type][$count]['image'] = (isset($account->picture->uri)) ? file_create_url($account->picture->uri) : "";
        $result['data'][$data_type][$count]['roles'] = (isset($account->roles)) ? $account->roles : "";
        $result['data'][$data_type][$count]['flat_number'] = common_get_field_data($account, 'field_flat_num', $lan_code);
        $result['data'][$data_type][$count]['room_number'] = common_get_field_data($account, 'field_room_num', $lan_code);
        if ($role_name == 'office_staff' || $role_name == 'field_staff') {
          $result['data'][$data_type][$count]['employer_name'] = common_get_field_data($account, 'field_employer_name', $lan_code);
          $result['data'][$data_type][$count]['employer_name_ar'] = common_get_field_data($account, 'field_employer_name_ar', $lan_code);
          $result['data'][$data_type][$count]['device_api‎'] = common_get_field_data($account, 'field_device_api‎', $lan_code);
        }
        if ($role_name == 'contract_customer' || $role_name == 'owned_customer') {
          $result['data'][$data_type][$count]['customer_name'] = common_get_field_data($account, 'field_customer_name', $lan_code);
          $result['data'][$data_type][$count]['customer_name_ar'] = common_get_field_data($account, 'field_customer_name_ar', $lan_code);
          $result['data'][$data_type][$count]['customer_id'] = common_get_field_data($account, 'field_customer_id', $lan_code);
          $result['data'][$data_type][$count]['occupant_name'] = common_get_field_data($account, 'field_occupant_name', $lan_code);
          $result['data'][$data_type][$count]['occupant_name_ar'] = common_get_field_data($account, 'field_occupant_name_ar', $lan_code);
          $result['data'][$data_type][$count]['postbox'] = common_get_field_data($account, 'field_postbox', $lan_code);
          $result['data'][$data_type][$count]['mobile'] = common_get_field_data($account, 'field_mobile', $lan_code);
          $result['data'][$data_type][$count]['fax'] = common_get_field_data($account, 'field_fax', $lan_code);
          $result['data'][$data_type][$count]['email'] = common_get_field_data($account, 'field_email', $lan_code);
          $result['data'][$data_type][$count]['country'] = common_get_field_data($account, 'field_country', $lan_code);
          $result['data'][$data_type][$count]['session_token'] = drupal_get_token('services');
        }
        $result['data'][$data_type][$count]['phone_res'] = common_get_field_data($account, 'field_phone_res', $lan_code);
        $result['data'][$data_type][$count]['phone_wrk'] = common_get_field_data($account, 'field_phone_wrk', $lan_code);
        $field_collection_items = array('passport', 'visa', 'license', 'power_attorney');
        foreach ($field_collection_items as $field_collection_item) {
          $item_id = common_get_field_data($account, 'field_' . $field_collection_item, $lan_code);
          $collection = entity_load('field_collection_item', array($item_id));
          $result['data'][$data_type][$count]['attachments'][$field_collection_item] = array();
          $result['data'][$data_type][$count]['attachments'][$field_collection_item]['number'] = common_get_field_data($collection[$item_id], 'number', $lan_code);
          $photos = common_get_field_data($collection[$item_id], 'attachment', $lan_code, 'filename', 'all');
          foreach ($photos as $index => $photo) {
            $result['data'][$data_type][$count]['attachments'][$field_collection_item]['files'][$index]['original'] = file_create_url($photo['uri']);
          }
          $result['data'][$data_type][$count]['attachments'][$field_collection_item]['issue_date'] = common_get_field_data($collection[$item_id], 'issue_date', $lan_code);
          $result['data'][$data_type][$count]['attachments'][$field_collection_item]['expiry_date'] = common_get_field_data($collection[$item_id], 'expiry_date', $lan_code);
          $result['data'][$data_type][$count]['attachments'][$field_collection_item]['remind_before'] = common_get_field_data($collection[$item_id], 'remind_before', $lan_code);
          if ($field_collection_item == 'passport') {
            $result['data'][$data_type][$count]['attachments'][$field_collection_item]['issue_place'] = common_get_field_data($collection[$item_id], 'issue_place', $lan_code);
          }
        }
      }
      $count++;
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _contract_customers_index()
{
  $role_name = 'contract_customer';
  $site_user = get_site_user_list_by_role($role_name);
  return $site_user;
}

function _owned_customers_index()
{
  $role_name = 'owned_customer';
  $site_user = get_site_user_list_by_role($role_name);
  return $site_user;
}

function _field_staff_index()
{
  $role_name = 'field_staff';
  $site_user = get_site_user_list_by_role($role_name);
  return $site_user;
}
function _office_staff_index()
{
  $role_name = 'office_staff';
  $site_user = get_site_user_list_by_role($role_name);
  return $site_user;
}

function _customers_index()
{
  $result = array();
  $contract_customers_user = _contract_customers_index();
  $owned_customers_user = _owned_customers_index();
  $data_type = 'users';
  if (array_key_exists('data', $contract_customers_user) && array_key_exists('data', $owned_customers_user)) {
    $result['data']['users'] = array_merge($contract_customers_user['data']['users'], $owned_customers_user['data']['users']);
  } else {
    if (array_key_exists('data', $contract_customers_user)) {
      $result['data']['users'] = $contract_customers_user['data']['users'];
    }
    if (array_key_exists('data', $owned_customers_user)) {
      $result['data']['users'] = $owned_customers_user['data']['users'];
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _employees_index()
{
  $result = array();
  $field_staff_user = _field_staff_index();
  $office_staff_user = _office_staff_index();
  $data_type = 'users';
  if (array_key_exists('data', $field_staff_user) && array_key_exists('data', $office_staff_user)) {
    $result['data']['users'] = array_merge($field_staff_user['data']['users'], $office_staff_user['data']['users']);
  } else {
    if (array_key_exists('data', $field_staff_user)) {
      $result['data']['users'] = $field_staff_user['data']['users'];
    }
    if (array_key_exists('data', $office_staff_user)) {
      $result['data']['users'] = $office_staff_user['data']['users'];
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _roles_index()
{
  global $language;
  $lan_code = $language->language;
  $data_type = 'user_roles';
  //Initialise result array
  $result = array();
  $count = 0;
  $result['data'][$data_type][$count] = array();
  $user_roles = user_roles();
  foreach ($user_roles as $key => $user_role) {
    $result['data'][$data_type][$count]['rid'] = $key;
    $result['data'][$data_type][$count]['name'] = $user_role;
    $count++;
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function get_tenancy_by_user($uid,$type = 'customer')
{
  $node_ids = array();
  //Entity Query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('type', "tenancy")
    ->propertyOrderBy('changed', "DESC")
    ->range(0, DEFAULT_PAGINATION_LIMIT);

  if( $type == 'customer') {
    $query->fieldCondition('field_customer', 'uid', $uid, '=');
  }

  $query_result = $query->execute();
  foreach ($query_result as $contents) {
    /* Append the list to the API output */
     $node_ids= array_keys($contents);
  }
  return $node_ids;
}
