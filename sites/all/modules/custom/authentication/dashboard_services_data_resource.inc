<?php
include_once 'authentication_services_data_resource.inc';
/**
 * @file
 * Common Module.
 */
function _dashboard_index($args) {
  $account = array();
  $count = 0;
  $data_type = 'dashboard';
  //Initialise result array
  $result = array();
  $result['data'][$data_type] = array();
  if (module_exists('authentication')) {
    $contract_customer_count = get_site_user_list_by_role('contract_customer','all',1);
    $owned_customer_count = get_site_user_list_by_role('owned_customer','all',1);
    $field_staff_count = get_site_user_list_by_role('field_staff','all',1);
    $office_staff_count = get_site_user_list_by_role('office_staff','all',1);
    $result['data'][$data_type]['users']['count'] = $contract_customer_count + $owned_customer_count;
    $result['data'][$data_type]['users']['contract_customer']['count'] = $contract_customer_count;
    $result['data'][$data_type]['users']['owned_customer']['count'] = $owned_customer_count;
    $result['data'][$data_type]['employees']['count'] = $field_staff_count + $office_staff_count;
    $result['data'][$data_type]['employees']['field_staff']['count'] = $field_staff_count;
    $result['data'][$data_type]['employees']['office_staff']['count'] = $office_staff_count;
  }
  if (module_exists('tenancy')) {
    $result['data'][$data_type]['tenancy']['count'] =  _get_entity_total_count('node','tenancy');
  }
  if (module_exists('building')) {
    $result['data'][$data_type]['building']['count'] =  _get_entity_total_count('node','building');
    $query_attributes['field'][0]['name'] = 'field_deed_type';
    $query_attributes['field'][0]['property'] = 'value';
    $query_attributes['field'][0]['identifier'] = '=';
    $query_attributes['field'][0]['value'] = '1'; // 1-Owned
    $result['data'][$data_type]['building']['owned']['count'] =  _get_entity_total_count('node','building',$query_attributes);
    $query_attributes['field'][0]['value'] = '2'; // 2-Leased
    $result['data'][$data_type]['building']['leased']['count'] =  _get_entity_total_count('node','building',$query_attributes);
    $query_attributes['field'][0]['value'] = '3'; // 3-Managed
    $result['data'][$data_type]['building']['managed']['count'] =  _get_entity_total_count('node','building',$query_attributes);
    $result['data'][$data_type]['flat']['count'] =  _get_entity_total_count('node','flat');
    $result['data'][$data_type]['rooms']['count'] =  _get_entity_total_count('node','rooms');
  }
  common_set_service_footer($result, $data_type,NULL,$message = "Successfully retrieved dashboard.",200);
  return $result;
}

function _get_entity_total_count($entity_type,$type,$query_attributes = array())
{
  $total_count = 0;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type)
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('type', $type);

  if(is_array($query_attributes) && !empty($query_attributes)) {
    foreach($query_attributes as $key =>$query_attribute) {
      if($key == 'field') {
        foreach($query_attribute as $attribute) {
          $query->fieldCondition($attribute['name'], $attribute['property'], $attribute['value'], $attribute['identifier']);
        }
      }
    }
  }
  $total_count = $query->count()->execute();

  return $total_count;
}
