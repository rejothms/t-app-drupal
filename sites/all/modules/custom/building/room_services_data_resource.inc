<?php

/**
 * @file
 * Bulding Module.
 */
function _room_index($list_type = 'all') {
  global $language;
  $lan_code = $language->language;
  $data_type = 'rooms';
  $args = $_REQUEST;
  $nid = common_get_array_user_data($args,'nid');
  $current_page = common_get_array_user_data($args, 'page');
  if($current_page == "" || !is_numeric($current_page)) {
    $current_page = 0;
  }
  //Initialise result array
  $result = array();
  //Entity Query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('type', "rooms")
      ->propertyOrderBy('changed',"DESC")
      ->range($current_page * DEFAULT_PAGINATION_LIMIT,DEFAULT_PAGINATION_LIMIT);

  if (isset($nid) && $nid != "") {
    $query->propertyCondition('nid', $nid);
  }

  $query_result = $query->execute();
  $count = 0;
  foreach ($query_result as $contents) {
    /* Append the list to the API output */
    $node_ids = array_keys($contents);
    /* Generate the list of orders */
    $count = 0;
    foreach ($node_ids as $node_id) {
      $node = node_load($node_id);
      $result['data'][$data_type][$count] = array();
      if($list_type == 'minimal') {
        $result['data'][$data_type][$count]['nid']= $node_id;
        $result['data'][$data_type][$count]['title']= $node->title;
      }
      else {
        $result['data'][$data_type][$count]['nid']= $node_id;
        $result['data'][$data_type][$count]['title']= $node->title;
        $result['data'][$data_type][$count]['room_number'] = common_get_field_data($node, 'field_room_num', $lan_code);
        $result['data'][$data_type][$count]['flat_nid'] = common_get_field_data($node, 'field_flat', $lan_code, 'nid');
        $result['data'][$data_type][$count]['building_nid'] = common_get_field_data($node, 'field_building',$lan_code, 'nid');
      }
      $count++;
    }
  }
  common_set_service_footer($result,$data_type,'list');
  return $result;
}

function _room_manipulate($args) {
  global $user;
  global $language;
  $language_code = $language->language;
  $type = "rooms";
  $result = array();
  $nid = isset($args['nid']) ? $args['nid'] : "";
  if($nid != "") {
    $node = node_load($nid);
    if (!empty($node)) {
      $node->uid = $user->uid;  //0 Anonymous User
      $action = 'update';
    }
  }
  else {
    $node = new stdClass();
    $node->type = $type;
    $node->status = 1; //(1 or 0): published or not
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    $node->language = $language_code; // Or e.g. 'en' if locale is enabled
    $node->uid = $user->uid;  //0 Anonymous User
    $action = 'add';
  }
  if(common_get_array_data($args, 'title') != "") {
    $node->title = common_get_array_data($args, 'title');
  }
  if(common_get_array_data($args, 'room_number') != "") {
    $node->field_room_num[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'room_number');
  }
  if(common_get_array_data($args, 'flat_nid') != "") {
    $node->field_flat[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'flat_nid');
  }
  if(common_get_array_data($args, 'building_nid') != "") {
    $node->field_building[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'building_nid');
  }

  node_object_prepare($node);
  node_save($node);

  if ($node->nid) {
    $new_node = node_load($node->nid);
    $result['data'][$type]['nid'] = $new_node->nid;
  }
  common_set_service_footer($result, $type, $action);
  return $result;
}

function _minimal_room_index() {
  $result = _room_index('minimal');
  return $result;
}
