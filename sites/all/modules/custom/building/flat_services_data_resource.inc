<?php

/**
 * @file
 * Bulding Module.
 */
function _flat_index($list_type = 'all') {
  global $language;
  $lan_code = $language->language;
  $data_type = 'flat';
  $args = $_REQUEST;
  $nid = common_get_array_user_data($args,'nid');
  $current_page = common_get_array_user_data($args, 'page');
  if($current_page == "" || !is_numeric($current_page)) {
    $current_page = 0;
  }
  //Initialise result array
  $result = array();
  //Entity Query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('type', "flat")
      ->propertyOrderBy('changed',"DESC")
      ->range($current_page * DEFAULT_PAGINATION_LIMIT,DEFAULT_PAGINATION_LIMIT);

  if (isset($nid) && $nid != "") {
    $query->propertyCondition('nid', $nid);
  }
  $query_result = $query->execute();
  $count = 0;
  foreach ($query_result as $contents) {
    /* Append the list to the API output */
    $node_ids = array_keys($contents);
    /* Generate the list of orders */
    $count = 0;
    foreach ($node_ids as $node_id) {
      $node = node_load($node_id);
      $result['data'][$data_type][$count] = array();
      if($list_type == 'minimal') {
        $result['data'][$data_type][$count]['nid']= $node_id;
        $result['data'][$data_type][$count]['title']= $node->title;
      }
      else {
        $result['data'][$data_type][$count]['nid']= $node_id;
        $result['data'][$data_type][$count]['title']= $node->title;
        $result['data'][$data_type][$count]['flat_number'] = common_get_field_data($node, 'field_flat_num', $lan_code);
        $result['data'][$data_type][$count]['floor'] = common_get_field_data($node, 'field_floor', $lan_code);
        $result['data'][$data_type][$count]['diva_num'] = common_get_field_data($node, 'field_diva_num', $lan_code);
        $result['data'][$data_type][$count]['parking_spot'] = common_get_field_data($node, 'field_parking_spot', $lan_code);
        $result['data'][$data_type][$count]['building_nid'] = common_get_field_data($node, 'field_building',$lan_code, 'nid');
      }
      $count++;
    }
  }
  common_set_service_footer($result,$data_type,'list');
  return $result;
}

function _flat_manipulate($args) {
  global $user;
  global $language;
  $language_code = $language->language;
  $type = "flat";
  $result = array();
  $nid = isset($args['nid']) ? $args['nid'] : "";
  if($nid != "") {
    $node = node_load($nid);
    if (!empty($node)) {
      $node->uid = $user->uid;  //0 Anonymous User
      $action = 'update';
    }
  }
  else {
    $node = new stdClass();
    $node->type = $type;
    $node->status = 1; //(1 or 0): published or not
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    $node->language = $language_code; // Or e.g. 'en' if locale is enabled
    $node->uid = $user->uid;  //0 Anonymous User
    $action = 'add';
  }
  if(common_get_array_data($args, 'title') != "") {
  $node->title = common_get_array_data($args, 'title');
  }
  if(common_get_array_data($args, 'flat_number') != "") {
  $node->field_flat_num[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'flat_number');
  }
  if(common_get_array_data($args, 'floor') != "") {
    $node->field_floor[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'floor');
  }
  if(common_get_array_data($args, 'diva_num') != "") {
    $node->field_diva_num[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'diva_num');
  }
  if(common_get_array_data($args, 'parking_spot') != "") {
    $node->field_parking_spot[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'parking_spot');
  }
  if(common_get_array_data($args, 'building_nid') != "") {
    $node->field_building[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'building_nid');
  }

  node_object_prepare($node);
  node_save($node);

  if ($node->nid) {
    $new_node = node_load($node->nid);
    $result['data'][$type]['nid'] = $new_node->nid;
  }
  common_set_service_footer($result, $type, $action);
  return $result;
}

function _minimal_flat_index() {
  $result = _flat_index('minimal');
  return $result;
}
