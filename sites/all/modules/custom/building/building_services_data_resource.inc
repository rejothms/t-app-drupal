<?php

/**
 * @file
 * Bulding Module.
 */
function _building_index($list_type = 'all')
{
  global $language;
  $lan_code = $language->language;
  $data_type = 'buildings';
  $args = $_REQUEST;
  $nid = common_get_array_user_data($args,'nid');
  $current_page = common_get_array_user_data($args, 'page');
  if($current_page == "" || !is_numeric($current_page)) {
    $current_page = 0;
  }
  //Initialise result array
  $result = array();
  //Entity Query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('type', "building")
    ->propertyOrderBy('changed',"DESC")
    ->range($current_page * DEFAULT_PAGINATION_LIMIT,DEFAULT_PAGINATION_LIMIT);

  if (isset($nid) && $nid != "") {
    $query->propertyCondition('nid', $nid);
  }

  $query_result = $query->execute();
  $count = 0;
  foreach ($query_result as $contents) {
    /* Append the list to the API output */
    $node_ids = array_keys($contents);
    /* Generate the list of orders */
    $count = 0;
    foreach ($node_ids as $node_id) {
      $node = node_load($node_id);
      $result['data'][$data_type][$count] = array();
      if ($list_type == 'minimal') {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
        $result['data'][$data_type][$count]['short_title'] = common_get_field_data($node, 'field_short_title', $lan_code);
      } else {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
        $result['data'][$data_type][$count]['short_title'] = common_get_field_data($node, 'field_short_title', $lan_code);
        $result['data'][$data_type][$count]['building_number'] = common_get_field_data($node, 'field_building_number', $lan_code);
        $result['data'][$data_type][$count]['building_type'] = common_get_field_data($node, 'field_building_type', $lan_code);
        $result['data'][$data_type][$count]['city'] = common_get_field_data($node, 'field_city', $lan_code);
        $result['data'][$data_type][$count]['country'] = common_get_field_data($node, 'field_country', $lan_code);
        $result['data'][$data_type][$count]['deed_type'] = common_get_field_data($node, 'field_deed_type', $lan_code);
        $result['data'][$data_type][$count]['landlord_name'] = common_get_field_data($node, 'field_landlord_name', $lan_code);
        $result['data'][$data_type][$count]['landlord_name_ar'] = common_get_field_data($node, 'field_landlord_name_ar', $lan_code);
        $result['data'][$data_type][$count]['plot_number'] = common_get_field_data($node, 'field_plot_number', $lan_code);
        $result['data'][$data_type][$count]['street'] = common_get_field_data($node, 'field_street', $lan_code);
      }
      $count++;
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _building_manipulate($args)
{
  global $user;
  global $language;
  $language_code = $language->language;
  $type = "building";
  $result = array();
  $nid = isset($args['nid']) ? $args['nid'] : "";
  if ($nid != "") {
    $node = node_load($nid);
    if (!empty($node)) {
      $node->uid = $user->uid;  //0 Anonymous User
      $action = 'update';
    }
  } else {
    $node = new stdClass();
    $node->type = $type;
    $node->status = 1; //(1 or 0): published or not
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    $node->language = $language_code; // Or e.g. 'en' if locale is enabled
    $node->uid = $user->uid;  //0 Anonymous User
    $action = 'add';
  }
  if (common_get_array_data($args, 'title') != "") {
    $node->title = common_get_array_data($args, 'title');
  }
  if (common_get_array_data($args, 'building_number') != "") {
    $node->field_building_number[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'building_number');
  }
  if (common_get_array_data($args, 'short_title') != "") {
    $node->field_short_title[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'short_title');
  }
  if (common_get_array_data($args, 'building_type') != "") {
    $node->field_building_type[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'building_type');
  }
  if (common_get_array_data($args, 'city') != "") {
    $node->field_city[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'city');
  }
  if (common_get_array_data($args, 'country') != "") {
    $node->field_country[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'country');
  }
  if (common_get_array_data($args, 'deed_type') != "") {
    $node->field_deed_type[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'deed_type');
  }
  if (common_get_array_data($args, 'landlord_name') != "") {
    $node->field_landlord_name[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'landlord_name');
  }
  if (common_get_array_data($args, 'landlord_name_ar') != "") {
    $node->field_landlord_name_ar[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'landlord_name_ar');
  }
  if (common_get_array_data($args, 'plot_number') != "") {
    $node->field_plot_number[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'plot_number');
  }
  if (common_get_array_data($args, 'street') != "") {
    $node->field_street[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'street');
  }

  node_object_prepare($node);
  node_save($node);

  if ($node->nid) {
    $new_node = node_load($node->nid);
    $result['data'][$type]['nid'] = $new_node->nid;
  }
  common_set_service_footer($result, $type, $action);
  return $result;
}

function _minimal_building_index()
{
  $result = _building_index('minimal');
  return $result;
}
