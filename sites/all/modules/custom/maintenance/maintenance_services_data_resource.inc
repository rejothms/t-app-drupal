<?php

/**
 * @file
 * Bulding Module.
 */
function _maintenance_index($list_type = 'all')
{
  global $language;
  $args = $_REQUEST;
  $lan_code = $language->language;
  $data_type = 'maintenance';
  $assigned_to = common_get_array_user_data($args, 'assigned_to');
  $customer = common_get_array_user_data($args,'customer');
  $nid = common_get_array_user_data($args,'nid');
  $current_page = common_get_array_user_data($args, 'page');
  if($current_page == "" || !is_numeric($current_page)) {
    $current_page = 0;
  }
  //Initialise result array
  $result = array();
  //Entity Query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('type', "maintenance")
    ->propertyOrderBy('changed',"DESC")
    ->range($current_page * DEFAULT_PAGINATION_LIMIT,DEFAULT_PAGINATION_LIMIT);

  if (isset($nid) && $nid != "") {
    $query->propertyCondition('nid', $nid);
  }
  if (isset($assigned_to) && $assigned_to != "") {
    $query->fieldCondition('field_employee', 'uid', $assigned_to, '=');
  }
  if (isset($customer) && $customer != "") {
    $query->fieldCondition('field_customer', 'uid', $customer, '=');
  }
  $query_result = $query->execute();
  $count = 0;
  foreach ($query_result as $contents) {
    /* Append the list to the API output */
    $node_ids = array_keys($contents);
    /* Generate the list of orders */
    $count = 0;
    foreach ($node_ids as $node_id) {
      $node = node_load($node_id);
      $result['data'][$data_type][$count] = array();
      if ($list_type == 'minimal') {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
      } else {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
        $building_nid = common_get_field_data($node, 'field_building', $lan_code, 'nid');
        $result['data'][$data_type][$count]['building'] = $building_nid;
        $flat_nid = common_get_field_data($node, 'field_flat', $lan_code, 'nid');
        $result['data'][$data_type][$count]['flat'] = $flat_nid;
        $room_nid = common_get_field_data($node, 'field_room', $lan_code, 'nid');
        $result['data'][$data_type][$count]['room'] = $room_nid;
        $result['data'][$data_type][$count]['tenancy'] = common_get_field_data($node, 'field_tenancy', $lan_code, 'nid');
        $result['data'][$data_type][$count]['maintenance_date'] = common_get_field_data($node, 'field_maintenance_date', $lan_code);
        $employee_id = common_get_field_data($node, 'field_employee', $lan_code, 'uid');
        $result['data'][$data_type][$count]['assigned_to'] = base64_encode($employee_id);
        $result['data'][$data_type][$count]['maintenance_type'] = common_get_field_data($node, 'field_maintenance_type', $lan_code);
        $result['data'][$data_type][$count]['customer_comments'] = common_get_field_data($node, 'field_customer_comments', $lan_code);
        $customer_id = common_get_field_data($node, 'field_customer', $lan_code, 'uid');
        $result['data'][$data_type][$count]['customer'] = base64_encode($customer_id);
        $result['data'][$data_type][$count]['customer_contact_number'] = common_get_field_data($node, 'field_customer_contact_number', $lan_code);;
        $result['data'][$data_type][$count]['customer_contact_time_from'] = common_get_field_data($node, 'field_customer_contact_time_from', $lan_code);
        $result['data'][$data_type][$count]['customer_contact_time_to'] = common_get_field_data($node, 'field_customer_contact_time_to', $lan_code);
        $maintenance_status = common_get_field_data($node, 'field_maintenance_status', $lan_code);
        $result['data'][$data_type][$count]['maintenance_status'] = common_get_field_data($node, 'field_maintenance_status', $lan_code);
        $result['data'][$data_type][$count]['maintenance_status_comment'] = common_get_field_data($node, 'field_maintenance_status_comment', $lan_code);
        $result['data'][$data_type][$count]['location'] = common_get_field_data($node, 'field_location', $lan_code);
        $result['data'][$data_type][$count]['latitude'] = common_get_field_data($node, 'field_latitude', $lan_code);
        $result['data'][$data_type][$count]['longitude'] = common_get_field_data($node, 'field_longitude', $lan_code);
        $result['data'][$data_type][$count]['attributes']['building_name'] = node_load($building_nid)->title;
        $result['data'][$data_type][$count]['attributes']['room_name'] = node_load($room_nid)->title;
        $result['data'][$data_type][$count]['attributes']['flat_name'] = node_load($flat_nid)->title;
        $result['data'][$data_type][$count]['attributes']['assigned_employee_name'] = get_site_user_details($employee_id, 'display_name');
        $result['data'][$data_type][$count]['attributes']['customer_name'] = get_site_user_details($customer_id, 'display_name');
        $maintenance_status_values = array(
          '0' => 'Not Assigned',
          '1' => 'Assigned',
          '2' => 'Accepted',
          '3' => 'Rejected',
          '4' => 'In Progress',
          '5' => 'Completed',
        );
        $result['data'][$data_type][$count]['attributes']['maintenance_status_name'] = $maintenance_status_values[$maintenance_status];
      }
      $count++;
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _maintenance_manipulate($args)
{
  global $user;
  global $language;
  $lan_code = $language->language;
  $assigned_employee_id = "";
  $type = "maintenance";
  $result = array();
  $nid = isset($args['nid']) ? $args['nid'] : "";
  if ($nid != "") {
    $node = node_load($nid);
    if (!empty($node)) {
      $node->uid = $user->uid;  //0 Anonymous User
      $action = 'update';
      $assigned_employee_id = common_get_field_data($node, 'field_employee', $lan_code, 'uid');
    }
  } else {
    $node = new stdClass();
    $node->type = $type;
    $node->status = 1; //(1 or 0): published or not
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write
    $node->language = $lan_code; // Or e.g. 'en' if locale is enabled
    $node->uid = $user->uid;  //0 Anonymous User
    $action = 'add';
  }

  if (common_get_array_data($args, 'title') != "") {
    $node->title = common_get_array_data($args, 'title');
  }
  if (common_get_array_data($args, 'building') != "") {
    $node->field_building[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'building');
  }
  if (common_get_array_data($args, 'flat') != "") {
    $node->field_flat[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'flat');
  }
  if (common_get_array_data($args, 'room') != "") {
    $node->field_room[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'room');
  }
  if (common_get_array_data($args, 'tenancy') != "") {
    $node->field_tenancy[LANGUAGE_NONE][0]['nid'] = common_get_array_data($args, 'tenancy');
  }
  if (common_get_array_data($args, 'maintenance_date') != "") {
    $node->field_maintenance_date[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'maintenance_date');
  }
  if (common_get_array_user_data($args, 'assigned_to') != "") {
    $node->field_employee[LANGUAGE_NONE][0]['uid'] = common_get_array_user_data($args, 'assigned_to');
  }
  if (common_get_array_data($args, 'maintenance_type') != "") {
    $node->field_maintenance_type[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'maintenance_type');
  }
  if (common_get_array_user_data($args, 'customer') != "") {
    $node->field_customer[LANGUAGE_NONE][0]['uid'] = common_get_array_user_data($args, 'customer');
  }
  if (common_get_array_data($args, 'customer_contact_number') != "") {
    $node->field_customer_contact_number[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_contact_number');
  }
  if (common_get_array_data($args, 'customer_comments') != "") {
    $node->field_customer_comments[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_comments');
  }
  if (common_get_array_data($args, 'customer_contact_time_from') != "") {
    $node->field_customer_contact_time_from[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_contact_time_from');
  }
  if (common_get_array_data($args, 'customer_contact_time_to') != "") {
    $node->field_customer_contact_time_to[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'customer_contact_time_to');
  }
  if (common_get_array_data($args, 'maintenance_status') != "") {
    $node->field_maintenance_status[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'maintenance_status');
  }
  if (common_get_array_data($args, 'maintenance_status_comment') != "") {
    $node->field_maintenance_status_comment[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'maintenance_status_comment');
  }
  if (common_get_array_data($args, 'location') != "") {
    $node->field_location[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'location');
  }
  if (common_get_array_data($args, 'latitude') != "") {
    $node->field_latitude[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'latitude');
  }
  if (common_get_array_data($args, 'longitude') != "") {
    $node->field_longitude[LANGUAGE_NONE][0]['value'] = common_get_array_data($args, 'longitude');
  }
  node_object_prepare($node);
  node_save($node);

  if ($node->nid) {
    $new_node = node_load($node->nid);
    $result['data'][$type]['nid'] = $new_node->nid;
    $current_assigned_employee_id = common_get_field_data($node, 'field_employee', $lan_code, 'uid');
    if($assigned_employee_id != $current_assigned_employee_id) {
      //Send notification out.
      $body = get_maintenance_details($new_node->nid);
      $notification_response = common_notification_send_data($body);
      $result['data'][$type]['notification_body'] = $body;
      $result['data'][$type]['notification_response'] = $notification_response;
    }
  }
  common_set_service_footer($result, $type, $action);
  return $result;
}


function get_maintenance_details($node_id) {
  global $language;
  $lan_code = $language->language;
  $node = node_load($node_id);
  $employee_id = common_get_field_data($node, 'field_employee', $lan_code, 'uid');
  $customer_id = common_get_field_data($node, 'field_customer', $lan_code, 'uid');
  $employee_name = get_site_user_details($employee_id, 'display_name');
  $employee_device_api = get_site_user_details($employee_id, 'device_api');
  $customer_name = get_site_user_details($employee_id, 'display_name');
  $customer_contact_number = common_get_field_data($node, 'field_customer_contact_number', $lan_code);
  $customer_comments = common_get_field_data($node, 'field_customer_comments', $lan_code);
  $customer_contact_time_from = common_get_field_data($node, 'field_customer_contact_time_from', $lan_code);
  $customer_contact_time_to = common_get_field_data($node, 'field_customer_contact_time_to', $lan_code);
  $maintenance_date = common_get_field_data($node, 'field_maintenance_date', $lan_code);
  $maintenance_type = common_get_field_data($node, 'field_maintenance_type', $lan_code);
  $location = common_get_field_data($node, 'field_location', $lan_code);
  $latitude = common_get_field_data($node, 'field_latitude', $lan_code);
  $longitude = common_get_field_data($node, 'field_longitude', $lan_code);

  if ($employee_device_api != "") {
    $body["to"] = $employee_device_api;
    $body["notification"]["title"] = 'Maintenance Request';
    if ($maintenance_date != "") {
      $body["notification"]["title"] .= " - ".$maintenance_date;
    }
    if ($maintenance_type != "") {
      $maintenance_type_values = array(
        '0' => 'Not Assigned',
        '1' => 'General maintenance',
        '2' => 'AC maintenance'
      );
      $body["notification"]["body"] = "Type of Maintenance:".$maintenance_type_values[$maintenance_type];
    }
    if ($customer_name != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Customer Name:".$customer_name;
    }
    if ($location != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Location:" . $location;
    }
    if ($latitude != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Latitude:" . $latitude;
    }
    if ($longitude != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Longitude:" . $longitude;
    }
    if ($customer_contact_number != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Customer Contact Name:".$customer_contact_number;
    }
    if ($customer_comments != "") {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Comments:".$customer_comments;
    }
    if ($customer_contact_time_from != "" && $customer_contact_time_to != "" ) {
      $body["notification"]["body"] .= "\n";
      $body["notification"]["body"] .= "Timing:".$customer_contact_time_from ."-".$customer_contact_time_to;
    } else {
      if ($customer_contact_time_from != "") {
        $body["notification"]["body"] .= "\n";
        $body["notification"]["body"] .= "Timing from:".$customer_contact_time_from;
      }
      if ($customer_contact_time_to != "") {
        $body["notification"]["body"] .= "\n";
        $body["notification"]["body"] .= "Timing Until:".$customer_contact_time_to;
      }
    }
    $body["priority"] = 10;
  }
  return $body;
}

function _minimal_maintenance_index()
{
  $result = _maintenance_index('minimal');
  return $result;
}

function _maintenance_status_index()
{
  global $language;
  $lan_code = $language->language;
  $data_type = 'maintenance_status';
  //Initialise result array
  $result = array();
  $count = 0;
  $result['data'][$data_type][$count] = array();
  $maintenance_status_values = array(
    '0' => 'Not Assigned',
    '1' => 'Assigned',
    '2' => 'Accepted',
    '3' => 'Rejected',
    '4' => 'In Progress',
    '5' => 'Completed',
  );
  foreach ($maintenance_status_values as $key => $maintenance_status_value) {
    $result['data'][$data_type][$count]['id'] = $key;
    $result['data'][$data_type][$count]['name'] = $maintenance_status_value;
    $count++;
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}

function _maintenance_not_assigned_index($list_type = 'all')
{
  global $language;
  $args = $_REQUEST;
  $lan_code = $language->language;
  $data_type = 'maintenance';
  //Initialise result array
  $result = array();
  //Entity Query
  $query = db_select('node', 'n');
  $query->leftjoin('field_data_field_employee', 'e', 'e.entity_id = n.nid');
  $query->condition('n.status', NODE_PUBLISHED, '=');
  $query->condition('n.type', "maintenance", '=');
  $query->condition('e.entity_id', null, 'is');
  $query->fields('n', array('nid'));
  $query->orderBy('n.changed', 'DESC');
  $query->range(0, DEFAULT_PAGINATION_LIMIT);
  $query_result = $query->execute();
  $count = 0;
  if ($query_result->rowCount()) {
    foreach ($query_result as $contents) {
    /* Generate the list of orders */
      $node_id = $contents->nid;
      $node = node_load($node_id);
      $result['data'][$data_type][$count] = array();
      if ($list_type == 'minimal') {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
      } else {
        $result['data'][$data_type][$count]['nid'] = $node_id;
        $result['data'][$data_type][$count]['title'] = $node->title;
        $building_nid = common_get_field_data($node, 'field_building', $lan_code, 'nid');
        $result['data'][$data_type][$count]['building'] = $building_nid;
        $flat_nid = common_get_field_data($node, 'field_flat', $lan_code, 'nid');
        $result['data'][$data_type][$count]['flat'] = $flat_nid;
        $room_nid = common_get_field_data($node, 'field_room', $lan_code, 'nid');
        $result['data'][$data_type][$count]['room'] = $room_nid;
        $result['data'][$data_type][$count]['tenancy'] = common_get_field_data($node, 'field_tenancy', $lan_code, 'nid');
        $result['data'][$data_type][$count]['maintenance_date'] = common_get_field_data($node, 'field_maintenance_date', $lan_code);
        $employee_id = common_get_field_data($node, 'field_employee', $lan_code, 'uid');
        $result['data'][$data_type][$count]['assigned_to'] = base64_encode($employee_id);
        $result['data'][$data_type][$count]['maintenance_type'] = common_get_field_data($node, 'field_maintenance_type', $lan_code);
        $result['data'][$data_type][$count]['customer_comments'] = common_get_field_data($node, 'field_customer_comments', $lan_code);
        $customer_id = common_get_field_data($node, 'field_customer', $lan_code, 'uid');
        $result['data'][$data_type][$count]['customer'] = base64_encode($customer_id);
        $result['data'][$data_type][$count]['customer_contact_number'] = common_get_field_data($node, 'field_customer_contact_number', $lan_code);;
        $result['data'][$data_type][$count]['customer_contact_time_from'] = common_get_field_data($node, 'field_customer_contact_time_from', $lan_code);
        $result['data'][$data_type][$count]['customer_contact_time_to'] = common_get_field_data($node, 'field_customer_contact_time_to', $lan_code);
        $maintenance_status = common_get_field_data($node, 'field_maintenance_status', $lan_code);
        $result['data'][$data_type][$count]['maintenance_status'] = common_get_field_data($node, 'field_maintenance_status', $lan_code);
        $result['data'][$data_type][$count]['maintenance_status_comment'] = common_get_field_data($node, 'field_maintenance_status_comment', $lan_code);
        $result['data'][$data_type][$count]['attributes']['building_name'] = node_load($building_nid)->title;
        $result['data'][$data_type][$count]['attributes']['room_name'] = node_load($room_nid)->title;
        $result['data'][$data_type][$count]['attributes']['flat_name'] = node_load($flat_nid)->title;
        $result['data'][$data_type][$count]['attributes']['assigned_employee_name'] = get_site_user_details($employee_id, 'display_name');
        $result['data'][$data_type][$count]['attributes']['customer_name'] = get_site_user_details($customer_id, 'display_name');
        $maintenance_status_values = array(
          '0' => 'Not Assigned',
          '1' => 'Assigned',
          '2' => 'Accepted',
          '3' => 'Rejected',
          '4' => 'In Progress',
          '5' => 'Completed',
        );
        $result['data'][$data_type][$count]['attributes']['maintenance_status_name'] = $maintenance_status_values[$maintenance_status];
      }
      $count++;
    }
  }
  common_set_service_footer($result, $data_type, 'list');
  return $result;
}
